<?php

include 'init.php';

$content = $_POST['content'] ?? NULL;
$date = $_POST['date'] ?? NULL;
$warn = $_POST['warn_days'] ?? NULL;
if (is_null($content) || is_null($date)) {
    redirect('index.php');
}

$subject = 'Event addition request received';

$str = 'Date: ' . $date . PHP_EOL;
$str .= 'Warn Days: ' . $warn . PHP_EOL;
$str .= 'Details: ' . $content . PHP_EOL;

emsg('S', 'Your event addition request has been sent.');
mail($cfg['calendar_owner'], $subject, $str);
redirect('index.php');
