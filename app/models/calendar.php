<?php

/**
 * @class calendar
 *
 * This class is meant for monthly calendars only. Anything else, and
 * you'll have to roll your own.
 *
 * The class is meant to be as modular and trouble-free as possible.
 * Instantiate the class. A single call to fetch_events(). Then a call
 * to build_calendar(). Then a call to output();
 *
 * This is the fifth generation of this class. The difference between
 * each generation is the source of the events. Originally, the class
 * relied upon the calendar program "remind(1)". In order to function
 * properly, the remind program had to run on the server. This was
 * problematic in shared hosting. Next generation was built on a
 * home-grown file format. It was not as capable as remind of expressing
 * event repetitions, but it worked. Next, I switched from that home
 * grown format to calcurse(1). An attempt was made to decode the
 * appointments file for calcurse, This worked marginally well, but the
 * it was hard to find documentation for all its features, and its
 * file format seemed to be rather chaotic. Generation 4 returned to
 * remind(1). Since this class would normally be operated on a local LAN
 * rather than on shared hosting, it was thought that this would be
 * satisfactory.
 *
 * The currnt iteration of this code contains a lot of code from prior
 * iterations which is not needed now, but is worth keeping around. This
 * iteration is not designed to allow the user to edit and delete and add
 * events on their own. Instead, it simply despatches the "calendar owner"
 * with requests by email to alter the calendar.
 *
 */

class calendar
{
	var $calendar_url, $caledit_url, $caladd_url;
	var $infile; // '/home/paulf/.calcurse/apts'
	var $today; // date class object
	var $start_date, $end_date; // ISO 8601 versions of starting and ending dates of the month
	var $start_dow; // starting day of the week for 1st day of month
	var $str; // output for the page
	var $events; // obvious
	var $cells; // the array of each days on the calendar, with events, etc.
	var $max_cells; // the number of cells in the calendar table
	var $max_events; // the number of events in the calendar
	var $rows_in_cal; // number of rows in the calendar table
	var $front_padding, $back_padding; // blank days before and after the actual calendar
	var $year, $month, $day; // today's date components

	/**
	 * CONSTRUCTOR
	 *
	 * Define all possible class values
	 * Based on the date fetched from $_GET, define related members
	 * Define members which derive from today and shape the calendar
	 */

	function __construct($event_file)
	{
		global $cfg;

		$this->infile = $event_file;

		// URLs are absolute, and contain the protocol
		// They are suitable for use with the anchor (<a>) tag
		$this->calendar_url = 'index.php?date=';
		$this->caledit_url = "evtedt.php";
		$this->caladd_url = 'evtadd.php';

		$this->cells = array();
		$this->events = array();

	}

    /*
     * SPECIAL NOTE REGARDING remind(1)
     *
     * It is possible to insert "reminders" which aren't actually events,
     * but which influence the way they are displayed on calendars, etc. I
     * interpret some of these. Here are some of the
     * possibilities. First is the reminder itself, then how it comes out
     * when the `remind -pp` command is run:
     *
     * REM 1 SPECIAL COLOR 128 0 0 Red
     * {"date":"2022-04-03", "filename":"/home/paulf/.reminders", "lineno":191, "passthru":"COLOR", "d":3, "priority":5000, "r":0, "g":128, "b":0, "rawbody":"Green", "body":"0 128 0 Green"}
     *
     * REM 2 SPECIAL SHADE 192 192 255
     * {"date":"2022-04-02", "filename":"/home/paulf/.reminders", "lineno":190, "passthru":"SHADE", "d":2, "priority":5000, "r":192, "g":192, "b":255, "body":"192 192 255"}
     *
     * REM [moondate(0)] SPECIAL MOON 0 -1 -1 [moontime(0)]
     * {"date":"2022-04-09", "filename":"/home/paulf/.reminders", "lineno":193, "passthru":"MOON", "d":9, "m":4, "y":2022, "nonconst_expr":1, "priority":5000, "rawbody":"1 -1 -1 [moontime(1)]", "body":"1 -1 -1 02:48"}
     *
     * REM Monday SPECIAL WEEK (W[weekno()])
     * {"date":"2022-04-04", "filename":"/home/paulf/.reminders", "lineno":196, "passthru":"WEEK", "wd":["Monday"], "priority":5000, "rawbody":"(W[weekno()])", "body":"(W14)"}
     *
     * REM 16 SPECIAL RANDOM-STUFF ignore me and be happy
     * {"date":"2022-04-16", "filename":"/home/paulf/.reminders", "lineno":198, "passthru":"RANDOM-STUFF", "d":16, "priority":5000, "body":"ignore me and be happy"}
     *
     * This is here for reference in later modifications of the code.
     */

	/**
	 * Fetch events from remind(1).
	 *
	 * This method is specifically designed to fetch events from a
	 * specifically formatted "remind(1)" command. It then formats the
	 * result for use by other calendar methods. The "remind -pp" command
     * outputs JSON, except for the first seven and last single lines.
	 *
	 * The resulting array looks like the following:
	 *
	 * array(
	 * array(
	 * 	'year' => 2018,
	 * 	'month' => 12,
	 * 	'day' => 15,
     * 	'warn' => 7,
	 * 	'filename' => '/home/paulf/.reminders',
	 * 	'lineno' => 84,
	 * 	'text' => 'Purchase hot dogs for BBQ'
	 * 	),
	 * array(
	 * 	'year' => 2018,
	 * 	'month' => 12,
	 * 	'day' => 16,
     * 	'warn' => 0,
	 * 	'filename' => '/home/paulf/.reminders',
	 * 	'lineno' => 85,
	 * 	'text' => 'BBQ at community center'
	 * 	),
	 * ...
	 * );	
     *
     * NOTE: Because remind(1) embeds some metadata in its events,
     * additional array members may exist in specific event arrays.
     *
     * NOTE: We assume that remind(1) returns events in date order.
	 *
	 * @param string ISO 8601 date string (YYYY-MM-DD)
	 *
	 * @return array 
	 *
	 */

	function fetch_events($datestr = NULL)
	{
		global $cfg;

        $dt = new xdate();
        $this->today = $dt;

        if (!is_null($datestr) && strlen($datestr) != 0) {
            $this->today->from_iso($datestr);
        }

        $this->year = $this->today->year;
        $this->month = $this->today->month;
        $this->day = $this->today->day;

		$start_of_month = $dt->from_ints($this->year, $this->month, 1);
		$this->rows_in_calendar($dt->day_of_week(), $dt->days_in_month());
		$this->max_cells = $this->rows_in_cal * 7;
		$this->calendar_padding($dt->day_of_week(), $this->rows_in_cal);

        $this->start_date = $dt->to_iso();

        $dt->from_ints($dt->year, $dt->month, $dt->days_in_month());
        $this->end_date = $dt->to_iso();

		if (is_null($datestr)) {
			$command = $cfg['executable'] . ' -pp ' . $this->infile;
		}
		else {
			$command = $cfg['executable'] . ' -pp ' . $this->infile . ' ' . $datestr;
		}

		exec($command, $result);

		$result_array = [];

		$max_lines = count($result);
		$result_array = [];
		// skip first seven lines; they're metadata
		for ($i = 7; $i < $max_lines; $i++) {

			$line = trim($result[$i]);

			if ($line == '# rem2ps2 end') {
				break;
			}

			$decoded = json_decode($line, TRUE, 512, 0);

			$year = (int) substr($decoded['date'], 0, 4);
			$month = (int) substr($decoded['date'], 5, 2);
			$day = (int) substr($decoded['date'], 8, 2);

			if (array_key_exists('passthru', $decoded)) {
				$passthru = $decoded['passthru'];
				switch ($passthru) {
					case 'SHADE':
                        // this shades the background of the cell
						$background = [$decoded['r'], $decoded['g'], $decoded['b']];
                        $result_array[] = [
                            'year' => $year,
                            'month' => $month,
                            'day' => $day,
                            'background' => $background
                        ];
						break;
					case 'COLOR':
                    case 'COLOUR':
                        // this paints the reminder in this color
						$color = [$decoded['r'], $decoded['g'], $decoded['b']];
                        // we preserve the message for colored reminders
                        $result_array[] = [
					        'filename' => $decoded['filename'],
					        'url' => str_replace('/', '%7E', $decoded['filename']),
                            'lineno' => $decoded['lineno'],
                            'year' => $year,
                            'month' => $month,
                            'day' => $day,
                            'color' => $color,
                            'text' => preg_replace('/^\d+\s+\d+\s+\d+\s+/', '', $decoded['body'])
                        ];
						break;
					case 'MOON':
                        // this makes a little moon graphic
						break;
					case 'WEEK':
                        // this adds a week number to the cell
                        $dt->from_ints($year, $month, $day);
                        $isoweek = $dt->week_of_year();

                        $result_array[] = [
					        'filename' => $decoded['filename'],
					        'url' => str_replace('/', '%7E', $decoded['filename']),
                            'lineno' => $decoded['lineno'],
                            'year' => (int) substr($decoded['date'], 0, 4),
                            'month' => (int) substr($decoded['date'], 5, 2),
                            'day' => (int) substr($decoded['date'], 8, 2),
                            'text' => "(Week: $isoweek)",
                        ];
						break;
					default:
                        // WHAT?
						break;
				}
			}
            else {
                $warn = $decoded['delta'] ?? '0';
				$result_array[] = [
					'filename' => $decoded['filename'],
					'url' => str_replace('/', '%7E', $decoded['filename']), 
					'lineno' => $decoded['lineno'],
					'year' => $year,
					'month' => $month,
					'day' => $day,
                    'warn' => (int) $warn,
					'text' => $decoded['body'],
				];
            }
		}
	
		$this->events = $result_array;
	}

	/**
	 * Number of rows in a calendar.
	 *
	 * Returns the number of rows in a calendar as dictated by the starting
	 * day of the week and the number of days in the month
	 *
	 * @param int $start_dow Starting day-of-week for first day in cal
	 * @param int $dim Days in month for this month
	 *
	 */

	function rows_in_calendar($start_dow, $dim)
	{
		if ($start_dow == 0 && $dim == 28)
			$rows = 4;
		elseif ($start_dow == 5 && $dim == 31)
			$rows = 6;
		elseif ($start_dow == 6 && $dim >= 30)
			$rows = 6;
		else
			$rows = 5;

		$this->rows_in_cal = $rows;
	}

	/**
	 * calendar_padding
	 *
	 * Sets the amount of pre- and post-padding (before and after the
	 * actual calendar dates) on a displayed calendar.
	 *
	 * @param int $start_dow Day number of the first day of the month
	 * (Sun = 0)
	 * @param int $rows Number of actual rows in displayed calendar
	 *
	 */

	function calendar_padding($start_dow, $rows)
	{
		$this->front_padding = $start_dow;
		$ndays = $rows * 7;
        $dim = $this->today->days_in_month();
		$this->back_padding = $ndays - ($start_dow + $dim);
	}

	/**
	 * key_values()
	 *
	 * This method returns a string where the first parameter is printed,
	 * followed by and equals sign (=) followed by the second parameter
	 * in double quotes. This is ubiquitous in HTML, and is used for that
	 * purpose. This string is automatically padded on the left by a
	 * space.
	 *
	 * @param string $key the HTML attribute
	 * @param string $value the value of the attribute
	 *
	 * @return string
	 */

	private function key_value($key, $value)
	{
		return ' ' . $key . '="' . $value . '"';
	}

	/**
     * Get the event array for a single event, by iterating through the
     * event arrays and finding a match.
	 *
     * @param string The filename of the event file
	 * @param integer Which line you want
	 *
	 * @return array event array or FALSE on failure
	 */

	function fetch_event($filename, $line_no)
	{
		if (empty($this->events)) {
			$this->fetch_events();
		}
		$max_events = count($this->events);
		for ($i = 0; $i < $max_events; $i++) {
            if (isset($this->events[$i]['filename']) && 
                isset($this->events[$i]['lineno']) &&
                $this->events[$i]['lineno'] == $line_no && 
                $this->events[$i]['filename'] == $filename) {

				return $this->events[$i];
				break;
			}
		}
		return FALSE;
	}

	/**
	 * calendar_instructions
	 *
	 * Generates the instructions (help) which appears above our
	 * calendar.
	 *
	 */

	function calendar_instructions()
	{
		$this->str = <<<EOT
<h3>
Click on any event to edit or delete it.<br>
Click on the prior or next month to go back or forward a month.<br>
Click on the prior or next year to go back or forward a year.<br>
Click on a month to go to that month<br>
Click on any date to add an event to that day.<br>
</h3>
EOT;
		$this->str .= PHP_EOL;
	}

	/**
	 * calendar_styles()
	 *
	 * Generates the style tags necessary to properly display our
	 * calendar.
	 *
	 */

	function calendar_styles()
	{

		$this->str .= <<<EOT
<style type="text/css">

.cal-pre-post { 
	background-color: #cccccc; 
} 

.cal-today {
	background-color: #ffffcc;
	vertical-align: top;
}

.cal-day-name {
	font-weight: bold;
	text-align: center;
	font-size: 12pt;
}

.cal-month-banner {
	font-size: 18pt;
	font-weight: bold;
	text-align: center;
	padding: 10pt 0pt 10pt 0pt;
}

.cal-day-number {
	vertical-align: top;
	text-decoration: none;
}

.cal-event {
	text-decoration: none;
}

.cal-nav {
	font-weight: bold;
	text-align: center;
}

.cal-pre-post {
	background-color: #cccccc;
}

.cal-day {
	vertical-align: top;
}

.cal-link {
	text-decoration: none;
}

</style>

EOT;

	}

	/**
	 * valid_day()
	 *
	 * Meant to handle edge cases. Normally, a day of the month between
	 * 1 and 31 would work, but for certain months (February, September,
	 * April, June, November) this day number must be changed because
	 * the months aren't that long (30 days or less). This routine
	 * changes the day passed in to one which fits the month and year.
	 *
	 * This routine assumes a valid positive integer for all three
	 * parameters and does not check for this. It also assumes the year
	 * and month numbers are reasonable values.
     *
     * The reason for this routine is that, in the navigation portion of
     * the calendar, links to before and after months/years include the
     * current day of the month (as in 2022-08-31). But if we jump to a
     * month with fewer days than the date we're passing, we must adjust
     * the date to a real one. So if we jump from 2022-08-31 to September
     * of that year (which has only 30 days), we adjust the date to
     * 2022-09-30.
	 *
	 * @param integer $year
	 * @param integer $month
	 * @param integer $day
	 *
	 * @return integer valid day of the month
	 *
	 */

	function valid_day($year, $month, $day)
	{
        $today = new xdate();
		$today->from_ints($year, $month, 1);
        $dim = $today->days_in_month();
		if ($day > $dim) {
			return $dim;
		}
		else {
			return $day;
		}
	}

	/**
	 * calendar_navigation
	 *
	 * Method returns a string containing the HTML for the navigational
	 * items at the top of our calendar, including the year and month
	 * links
	 */

	function calendar_navigation()
	{
		$short_months = array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
		$month_names = array('January', 'February', 'March', 'April', 'May', 'June',
			'July', 'August', 'September', 'October', 'November', 'December');
		$day_names = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');

		// Calendar column legends

		// Navigation outside this month
		$lastyear = $this->year - 1;
		$nextyear = $this->year + 1;

		$calnav = array();
		// left hand year
		$dt0 = $lastyear . '-' . $this->month . '-' . $this->valid_day($lastyear, $this->month, $this->day);
		$calnav[0] = '<a href="' . $this->calendar_url . $dt0 . '">' . $lastyear . '</a>';
		for ($i = 1; $i < 13; $i++) {
			// months
			$dtn = $this->year . '-' . $i . '-' . $this->valid_day($this->year, $i, $this->day);
			$calnav[$i] = '<a href="' . $this->calendar_url . $dtn . '">' . $short_months[$i - 1] . '</a>';
		}

		// right hand year
		$dt13 = $nextyear . '-' . $this->month  . '-' . $this->valid_day($nextyear, $this->month, $this->day);

		$calnav[13] = '<a href="' . $this->calendar_url . $dt13 . '">' . $nextyear . '</a>';

		// actual navs

		$this->str .= '<!-- actual navs -->' . PHP_EOL;
		$this->str .= '<tr>' . PHP_EOL;

		for ($i = 0; $i < 14; $i++) {
			$this->str .= '<td' . $this->key_value('class', 'cal-nav') . '>' . $calnav[$i] . '</td>' . PHP_EOL;
		}

		$this->str .= '</tr>' . PHP_EOL;

		// month banner

		$this->str .= '<!-- month banner -->' . PHP_EOL;
		$this->str .= '<tr>' . PHP_EOL;

		$this->str .=	'<td ' . $this->key_value('class', 'cal-month-banner') . $this->key_value('colspan', '14') . '>' . PHP_EOL;
		$this->str .= $month_names[$this->month - 1] . ' ' . $this->year . PHP_EOL;
		$this->str .=	'</td>' . PHP_EOL;
		$this->str .= '</tr>' . PHP_EOL;

		// days of week

		$this->str .= '<!-- days of week -->' . PHP_EOL;
		$this->str .= '<tr>' . PHP_EOL;

		for ($i = 0; $i < 7; $i++) {
			$this->str .= '<td' . $this->key_value('class', 'cal-day-name') . $this->key_value('colspan', '2') . '>' . $day_names[$i] . '</td>' . PHP_EOL;
		}

		$this->str .= '</tr>' . PHP_EOL;
		$this->str .= '<!-- End of navigation -->' . PHP_EOL;
	}


	/**
	 * Build calendar cells
	 *
	 * This method builds the array of cells which will be needed later.
	 * These cells encompass every slot on the displayed calendar, and
	 * contain all values necessary to display proper contents in each
	 * slot.
     *
     * The cells array looks like:
     * $this->cells = [
     *      [
     *          'dom' => 1,
     *          'row' => 1,
     *          'col' => 4,
     *          'background' => [240, 124, 36],
     *          'events' => [
     *              [
     *                  'filename' => '/home/paulf/.reminders',
     *                  'lineno' => 23,
     *                  'year' => 2022,
     *                  'month' => 4,
     *                  'day' => 1,
     *                  'text' => 'Get busy doing something!'
     *              ],
     *              [...]
     *      ],
     *      [...]
     * ];
     *
     * After this step, "build_calendar()" takes this data and creates the
     * actual string which gets displayed on screen. Actually,
     * "build_calendar()" calls this routine.
     *
	 */

	function make_cells()
	{
        $dim = $this->today->days_in_month();
		$day_of_month = 1;
		for ($a = 0; $a < $this->max_cells; $a++) {
			if ($a < $this->front_padding || $a >= $dim + $this->front_padding) {
				// areas before and after the main calendar
				$this->cells[$a]['dom'] = 0;
				$this->cells[$a]['row'] = floor($a / 7);
				$this->cells[$a]['col'] = $a % 7;
			}
			else {
				$this->cells[$a]['dom'] = $day_of_month;
				$this->cells[$a]['row'] = floor($a / 7);
				$this->cells[$a]['col'] = $a % 7;

				$max_events = count($this->events);
				for ($b = 0; $b < $max_events; $b++) {
					if ($this->events[$b]['day'] < $day_of_month) {
						continue;
					}
					elseif ($this->events[$b]['day'] > $day_of_month) {
                        // events are in date order; if we're past the
                        // current day of the month, don't go further
						break;
					}
					else {
						// this event belongs to today	
						if (array_key_exists('background', $this->events[$b])) {
							$this->cells[$a]['background'] = $this->events[$b]['background'];
						}
						else {
							$this->cells[$a]['events'][] = $this->events[$b];
						}
					}
				}

				$day_of_month++;
			}
		}
	}

	/**
	 * build_calendar()
	 *
	 * This routine returns the full HTML of the monthly calendar.
	 *
	 * @return string
	 */

	function build_calendar()
	{
		$this->make_cells();

		$this->calendar_instructions();
		$this->calendar_styles();

		$this->str .= '<p>' . PHP_EOL . '<div' . $this->key_value('align', 'center') . '>' . PHP_EOL;

		// start of table

		$this->str .= '<!-- Start of table -->' . PHP_EOL;

		$this->str .= '<table rules="all" border="1">' . PHP_EOL;

		$this->calendar_navigation();

		$this->str .= '<!-- Slots -->' . PHP_EOL;

		for ($b = 0; $b < $this->max_cells; $b++) { 

			$rem = $b % 7; 

			if ($rem == 0) { 
				$this->str .= '<tr>' . PHP_EOL;
			}

			if ($this->cells[$b]['dom'] > 0) {

				if ($this->cells[$b]['dom'] == $this->day) {
					// special background if it's today's day of the month
					$this->str .= '<td' . $this->key_value('colspan', '2') . $this->key_value('class', 'cal-today') . '>' . PHP_EOL;
				}
				elseif (isset($this->cells[$b]['background'])) {
					$rgb = $this->cells[$b]['background'];
					$background = ' style="background-color: rgb(' . $rgb[0] . ', ' . $rgb[1] . ', ' . $rgb[2] . ')"';
					$this->str .= '<td' . $this->key_value('colspan', '2') . $this->key_value('class', 'cal-day') . $background . '>' . PHP_EOL;
				}
				else {
					$this->str .= '<td' . $this->key_value('colspan', '2') . $this->key_value('class', 'cal-day') . '>' . PHP_EOL;
				}

				$req_str = "{$this->year}-{$this->month}-{$this->cells[$b]['dom']}";
				$this->str .= '<a' . $this->key_value('href', $this->caladd_url . '?date='. $req_str) . $this->key_value('class', 'cal-day-number') . '>' . $this->cells[$b]['dom'] . '</a><br/>' . PHP_EOL;

				if (isset($this->cells[$b]['events'])) {
					$max_e = count($this->cells[$b]['events']);
					for ($c = 0; $c < $max_e; $c++) {
						if ($c >= 1) {
							$this->str .= '<hr/>' . PHP_EOL;
						}

						if (array_key_exists('color', $this->cells[$b]['events'][$c])) {
							$color = sprintf(' style="color: #%02x%02x%02x"',
                                             $this->cells[$b]['events'][$c]['color'][0],
                                             $this->cells[$b]['events'][$c]['color'][1],
                                             $this->cells[$b]['events'][$c]['color'][2]);
						}
						else {
							$color = '';
						}

						$edit_link = $this->caledit_url . '?file=' . $this->cells[$b]['events'][$c]['url'] . '&line=' . $this->cells[$b]['events'][$c]['lineno'];

						$this->str .= '<a' . $this->key_value('href', $edit_link) . $this->key_value('class', 'cal-link') . $color . '>';
						$this->str .= $this->cells[$b]['events'][$c]['text'];
						$this->str .= '</a>' . PHP_EOL;

					}
				}
			}
			else {
				$this->str .= '<td' . $this->key_value('colspan', '2') . $this->key_value('class', 'cal-pre-post') . '>' . PHP_EOL;
			}

			$this->str .= '</td>' . PHP_EOL;

			if ($rem == 6) {	
				$this->str .= '</tr>' . PHP_EOL;
			}

		}
		
		$this->str .= '<!-- End Slots -->' . PHP_EOL;
		$this->str .= '</table>' . PHP_EOL;
	}

	function output()
	{
		echo $this->str;
	}

	function version()
	{
		return 5;
	}

    // THIS CODE IS OBSOLETE BUT STILL WORKS

	/**
	 * swap()
	 *
	 * This is a swap routine designed for use with PHP.
	 * It is uniquely tuned for use with indexed PHP arrays.
	 * It is called by the bubble_sort() method
	 *
	 * @param array $list The list of items to be sorted.
	 * @param integer $index1 The lesser index of the two to be swapped
	 * @param integer $index2 The greater index of the two to be swapped
	 * 
	 * @return array The sorted list
	 */

	function swap($list, $index1, $index2)
	{
		$item = $list[$index1];
		$list[$index1] = $list[$index2];
		$list[$index2] = $item;

		return $list;
	}

	/**
	 * compare()
	 *
     * Used by bubble_sort() routine.
     *
	 * Routine useful here with array comparisons; it is custom built
	 * for our event array.
	 *
	 * @param integer $item1 The lesser index of the two to be swapped
	 * @param integer $item2 The greater index of the two to be swapped
	 * 
	 * @return integer -1 less than, 0 same as, 1 greater than
	 */

	function compare($item1, $item2)
	{
		if ($item1['date']['j'] > $item2['date']['j'])
			return 1;
		elseif ($item1['date']['j'] < $item2['date']['j'])
			return -1;
		elseif ($item1['date']['j'] == $item2['date']['j'])
			return 0;
	}

	/**
	 * bubble_sort
	 *
     * THIS FUNCTION IS NEVER CALLED.
     *
	 * Used to sort an indexed array
	 * This method calls methods compare() and swap()
	 *
	 * @param array Indexed list to be sorted
	 *
	 * @return array The sorted array
	 *
	 */

	function bubble_sort($list)
	{
		$max_items = count($list);
		do {
			$n = 0;
			for ($i = 1; $i <= $max_items - 1; $i++) {
				if ($this->compare($list[$i - 1], $list[$i]) > 0) {
					$list = $this->swap($list, $i - 1, $i);
					$n = $i;
				}
			}
			$max_items = $n;
		} while ($max_items != 0);

		return $list;
	}


} // end of class

