<?php include VIEWDIR . 'head.view.php'; ?>
<form action="<?php echo $return; ?>" method="post">

<h3>
Indicate what you'd like to communicate below.<br/>
Be as clear as possible; your programmer does not read minds.
</h3>

<table>
<?php $form->show(); ?>
</table>

</form>

<?php include VIEWDIR . 'foot.view.php'; ?>
