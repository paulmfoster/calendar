<?php include VIEWDIR . 'head.view.php'; ?>

<form method="post" action="<?php echo $return; ?>">
<?php
$form->hidden('file');
$form->hidden('line');
$form->hidden('original');
?>
<h3>Specify changes you wish made, or select "Delete".</h3>
<p>
<?php $form->textarea('instructions'); ?>
</p>
<?php $form->submit('edit'); ?>
&nbsp;
<?php $form->submit('delete'); ?>
&nbsp;
<?php form::abandon('index.php'); ?>

</form>
<?php include VIEWDIR . 'foot.view.php'; ?>
