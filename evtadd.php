<?php

include 'init.php';

$date = $_GET['date'] ?? NULL;

$dt = new xdate();
if (!is_null($date)) {
    $parts = explode('-', $date);
    $dt->from_ints($parts[0], $parts[1], $parts[2]);
}

$date = $dt->to_iso();

$form = load('form');
$fields = [
    'date' => [
        'name' => 'date',
        'type' => 'date',
        'value' => $date,
        'required' => 1
    ],
    'warn_days' => [
        'name' => 'warn_days',
        'type' => 'text',
        'size' => '4',
        'maxlength' => '4'
    ],
    'content' => [
        'name' => 'content',
        'type' => 'textarea',
        'rows' => 10,
        'cols' => 50,
        'required' => 1
    ],
    's1' => [
        'name' => 's1',
        'type' => 'submit',
        'value' => 'Send'
    ]
];
$form->set($fields);
$page_title = 'Add Event';
$return = 'evtadd2.php';
include VIEWDIR . 'evtadd.view.php';

