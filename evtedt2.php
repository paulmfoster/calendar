<?php

include 'init.php';

$edit = $_POST['edit'] ?? NULL;
$delete = $_POST['delete'] ?? NULL;
$original = $_POST['original'] ?? NULL;

if (!is_null($edit)) {
    $subject = 'Event edit request received';
    $str = 'File: ' . $_POST['file'] . PHP_EOL;
    $str .= 'Line: ' . $_POST['line'] . PHP_EOL;
    $str .= 'Original: ' . $_POST['original'] . PHP_EOL;
    $str .= 'New Edit: ' . $_POST['instructions'] . PHP_EOL;
    mail($cfg['calendar_owner'], $subject, $str);
    emsg('S', 'Edit proposals sent to calendar owner.');
}
elseif (!is_null($delete)) {
    $subject = 'Event deletion request received';
    $str = 'File: ' . $_POST['file'] . PHP_EOL;
    $str .= 'Line: ' . $_POST['line'] . PHP_EOL;
    $str .= 'Existing content: ' . $_POST['original'] . PHP_EOL;
    mail($cfg['calendar_owner'], $subject, $str);
    emsg('S', 'Deletion request sent to calendar owner');
}

redirect('index.php');

