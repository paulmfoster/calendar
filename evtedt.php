<?php

include 'init.php';

$file = $_GET['file'] ?? NULL;
$line = $_GET['line'] ?? NULL;

if (is_null($file) || is_null($line)) {
    redirect('index.php');
}

$file = str_replace('~', '/', $file);
$form = load('form');

$event = $cal->fetch_event($file, $line);

$fields = [
    'file' => [
        'name' => 'file',
        'type' => 'hidden',
        'value' => $file
    ],
    'line' => [
        'name' => 'line',
        'type' => 'hidden',
        'value' => $line
    ],
    'original' => [
        'name' => 'original',
        'type' => 'hidden',
        'value' => $event['text']
    ],
    'instructions' => [
        'name' => 'instructions',
        'type' => 'textarea',
        'value' => $event['text'],
        'rows' => 10,
        'cols' => 50
    ],
    'edit' => [
        'name' => 'edit',
        'type' => 'submit',
        'value' => 'Save Edits'
    ],
    'delete' => [
        'name' => 'delete',
        'type' => 'submit',
        'value' => 'Delete'
    ]
];

$form->set($fields);
$page_title = 'Edit/Delete Event';
$focus_field = 'instructions';
$return = 'evtedt2.php';
include VIEWDIR . 'evtedt.view.php';

