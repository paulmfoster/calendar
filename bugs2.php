<?php

include 'init.php';

$send = $_POST['send'] ?? NULL;
if (is_null($send)) {
    redirect('index.php');
}

if (empty($_POST['email'])) {
    emsg('F', 'You must include a name and email');
}
elseif (empty($_POST['remark'])) {
    emsg('F', 'No comment = no response. Aborted.');
}
else {
    $msg = 'Application = ' . $cfg['app_name'] . "\n";
    $msg .= 'Name = ' . $_POST['name'] . "\n";
    $msg .= 'Email = ' . $_POST['email'] . "\n\n";
    $msg .= 'Remarks = ' . $_POST['remark'] . "\n\n";

    mail($cfg['programmer_email'], 'Bug Report or Feature Request for ' . $cfg['app_name'] , $msg);
    emsg('S', 'Thanks for your feedback. It is appreciated.');
}

redirect('index.php');

