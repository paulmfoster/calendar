<?php

include 'init.php';

$form = load('form');
$fields = [
    'app_title' => [
        'name' => 'app_title',
        'type' => 'hidden',
        'value' => $cfg['app_name']
    ],
    'name' => [
        'name' => 'name',
        'type' => 'text',
        'size' => 50,
        'maxlength' => 50,
        'label' => 'Name'
    ],
    'email' => [
        'name' => 'email',
        'type' => 'text',
        'size' => 50,
        'maxlength' => 50,
        'required' => 1,
        'label' => 'Email'
    ],
    'remark' => [
        'name' => 'remark',
        'type' => 'textarea',
        'rows' => 20,
        'cols' => 50,
        'size' => 1024,
        'required' => 1,
        'label' => 'Request'
    ],
    'send' => [
        'name' => 'send',
        'type' => 'submit',
        'value' => 'Send'
    ]
];

$form->set($fields);
$page_title = 'Bug Report/Feature Request';
$return = 'bugs2.php';
$focus_field = 'name';
include VIEWDIR . 'bugs.view.php';

