<?php

/**
 * @copyright 2018 Paul M. Foster <paulf@quillandmouse.com>
 * @author Paul M. Foster <paulf@quillandmouse.com>
 */

// define system directories
define('SYSDIR', 'system/');
define('INCDIR', SYSDIR . 'includes/');
define('LIBDIR', SYSDIR . 'libraries/');

// define application directories
define('APPDIR', 'app/');
define('CFGDIR', APPDIR . 'config/');
define('PRINTDIR', APPDIR . 'printq/');
define('IMGDIR', APPDIR . 'images/');
define('DATADIR', APPDIR . 'data/');
define('MODELDIR', APPDIR . 'models/');
define('VIEWDIR', APPDIR . 'views/');
define('CTLDIR', APPDIR . 'controllers/');

// provide common utilities
include INCDIR . 'utils.inc.php';

$cfgfile = CFGDIR . 'config.ini';
if (!file_exists($cfgfile)) {
    die('<code>' . $cfgfile . '</code> file not present. Aborting.');
}
$cfg = parse_ini_file(CFGDIR . 'config.ini');

session_start();

load('errors');
load('messages');
load('xdate');
$nav = load('navigation');

$cal = model('calendar', $cfg['input_file']);

$nav_links = [
	'Today' => 'index.php',
	'Add Event' => 'evtadd.php',
	'Bug Report/Feature Request' => 'bugs.php'
];

$navlinks = $nav->init('L', $nav_links);

