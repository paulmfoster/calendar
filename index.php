<?php

include 'init.php';

$date = $_GET['date'] ?? NULL;

$cal->fetch_events($date);
$cal->build_calendar();
$page_title = 'Show Calendar';
include VIEWDIR . 'index.view.php';

