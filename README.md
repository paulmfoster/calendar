# Calendar

This is a web front end for the Remind program. See

[https://dianne.skoll.ca/projects/remind/]

You edit your reminders file on the back end. You run this on your local
web server to see a calendar of the current month (or any month).

Simply install the software in any convenient directory served by your
web server. Edit the `config/config.ini` file; the values there should
be obvious.

Obviously, you must be running Remind, and you must have a reminders
file. This software will run Remind in the background to create a
secondary file which the calendar software can read, and use to build a
calendar web page.

You can't add, change or delete events from this program. But there are
pages in this software which will accept requests from you, and mail
them to the person running the Remind program.

In my family, I maintain the reminders via Remind. The Calendar program
allows my wife to see what events are scheduled for the month.

This software is licensed under the GNU Public License, version 2. The
text of that license is included with this package.

